package parents;

public class Person {
	public String name;
	public String addrest;
	
	public Person() {
		
	}
	
	public Person(String name, String addrest) {
		super();
		this.name = name;
		this.addrest = addrest;
	}
	
	public void greeting() {
		System.out.println("Helo my name "+ name +".");
		System.out.println("I, come from "+ addrest +".");
	}
}
