package childs;
import parents.Person;

public class Programmer extends Person {
	public String tecknology;
	
	public Programmer() {
		
	}
	
	public Programmer(String name, String addrest, String tecknology) {
		super(name, addrest);
		this.tecknology = tecknology;
	}

	public void hacking() {
		System.out.println("I can hacking a website");
	}
	
	public void coding() {
		System.out.println("i can coding "+tecknology+".");
	}
	
	public void greeting() {
		super.greeting();
		System.out.println("My job is a "+tecknology+"programmer.");
	}
}
