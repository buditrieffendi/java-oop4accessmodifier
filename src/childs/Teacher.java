package childs;
import parents.Person;

public class Teacher extends Person {
	public String subject;
	
	public Teacher(){
		
	}
	
	public Teacher(String name, String addrest, String subject) {
		super(name, addrest);
		this.subject = subject;
	}

	public void teaching() {
		System.out.println("I can teact "+subject+".");
	}
	
	public void greeting() {
		super.greeting();
		System.out.println("My job is a "+subject+".");
	}
}
